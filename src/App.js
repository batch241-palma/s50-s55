import './App.css';

import { useState, useEffect } from 'react';

import {UserProvider} from './UserContext';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';

import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import { Container } from 'react-bootstrap';



function App() {

  // This will be used to store user information that will be used for validating if a user is logged in on the app or not
  // State hook for the user state that is defined for global state
  // const [user, setUser] = useState({email: localStorage.getItem('email')})
  const [user, setUser] = useState({
    // allows us to store and to access the properties in the "user ID" and "isAdmin" data.
    id: null,
    isAdmin: null
    });

  const unsetUser = () => {
    localStorage.clear();
  } 

  // Used to check if the user information is properly stored upon login and properly cleared upon logout
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      // User is logged in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        // User is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  return (
    /*Fragments - common pattern in React.js for a component to return multiple elements*/
    <>
    {/*Initializes that dynamic routing will be involved*/}
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
            <Routes>
              <Route path="/" element={<Home/>} />
              <Route path="/courses" element={<Courses/>} />
              <Route path="/courses/:courseId" element={<CourseView/>} />
              <Route path="/register" element={<Register/>} />
              <Route path="/login" element={<Login/>} />  
              <Route path="/logout" element={<Logout/>} />
              {/*
              "*" - is a wildcard character that will match any path that was not declared in the previous routes.
              */}
              <Route path="/*" element={<Error/>} />  
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
  );
}

export default App;


