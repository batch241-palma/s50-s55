import { useState, useEffect } from 'react';

import { Link } from 'react-router-dom';

// S50 ACTIVITY
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({course}) {

	// const {name, description, price} = course;
	// S51 ACTIVITY
	const {name, description, price, _id} = course;

	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information related to individual components
	/*
		SYNTAX
		const [getter, setter] = useState(initialGetterValue)
	*/
	// const [count, setCount] = useState(0);
	// console.log(useState);
	// S51 ACTIVITY
	// const [seats, setSeats] = useState(30);
	// const [isOpen, setIsOpen] = useState(true);

	// function that keeps track of the enrolles for a course
	// function enroll() {
	// 	setCount(count + 1);

		// S51 ACTIVITY
		// setSeats(seats - 1);
		// S51 ACTIVITY
		// if(seats === 1){
		// 	alert("No more seats.")
		// 	document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
		// }
	// }
	// useEffect() - will allow us to execute a function if the value of seats state changes
	// useEffect(() => {
	// 	if(seats === 0) {
	// 		setIsOpen(false);
	// 		document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
	// 	}
	// 	// will run anytime one of the values in the array of the depencies changes
	// }, [seats])


	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        {/*<Card.Text>Enrollees: {count} </Card.Text>*/}
	        {/*<Button variant="primary">Enroll</Button>*/}
	        {/*<Button className="bg-primary" onClick={enroll}>Enroll</Button>*/}
	        {/*S51 ACTIVITY*/}
	       {/* <Card.Text>Seats: {seats}</Card.Text>
	        <Button id={`btn-enroll-${id}`} className="bg-primary" onClick={enroll}>Enroll</Button>*/}
	        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
	    </Card.Body>
	</Card>
	)
}

