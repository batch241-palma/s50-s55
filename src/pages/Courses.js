import {useState, useEffect} from 'react';

// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){

	// State that will be used to store the courses retrieved from the database
	const[courses, setCourses] = useState([]);

	// Retrieves the courses fromt the database upon initial render of the "Courses component"
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {
				return (
				<CourseCard key={course.id} course ={course} />
				)
			}))
		})
	}, []);

	// Checks to see if the mock data was captured
	// console.log(coursesData);
	// console.log(coursesData[0]);

	// The "map" method loops through the individual course object in our array and retuns a component for each course
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData aray using the "courseProp"
	// const courses = coursesData.map(course => {
	// 	return (
	// 		/* <CourseCard key={course.id} courseProp ={course} /> */
	// 		<CourseCard key={course.id} course ={course} />
	// 	)
	// })

	// Props Drilling - allows us to pass information from one component to another using "props"
	// Curly braces {} are used for props to signify that we are providing/passing information
	return (
		<>
		{courses}
		{/*<CourseCard courseProp ={coursesData[0]} />*/}
		</>
	)
}
