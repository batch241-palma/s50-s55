// S52 Activity
import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';

import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(){

    // Allows us to use the UserContext object and its properties to be used for user validation
    const {user, setUser} = useContext(UserContext);

    // State hooks to store values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')
    // State to determine whether submit button will be enabled or not
    const [isActive, setIsActive] = useState(false);

    // hook returns function that lets you navigate to components
    // const navigate = useNavigate();

    function authenticate(e){
        // Prevents page from reloading
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            // If no user information is found, the "access" property will not be available and return undefined

            if(typeof data.access !== "undefined"){
                // The JWT will be used to retrieve user information accross the whole frontend application and storing it in the localStorage
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            } else {
                    Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                    })
                }
            })

        // Set the email of the authenticated user in local storage
        // localStorage.setItem('propertyName', value)
        // localStorage.setItem('email', email);
        // Sets the global user state to have properties obtained from local storage
        // setUser({email: localStorage.getItem('email')});

        // Clear input fields after submission
        setEmail('');
        setPassword('');
        // navigate('/');
        
        // alert(`${email} has been verified! Welcome back!`)
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            // Global user state for validation accross the whole app
            // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation accross whole application
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        // Validation to enable submit button when all fields are populated.
        if(email !== '' && password !==''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    }, [email, password])

    return (
        (user.id !== null) ?
        <Navigate to ="/courses"/>
        :
        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

             { isActive ?
                <Button variant="success" type="submit" id="submitBtn">
                Login
                </Button>
            :
                <Button variant="success" type="submit" id="submitBtn" disabled>
                Login
                </Button>
            }
        </Form>
    )
}
