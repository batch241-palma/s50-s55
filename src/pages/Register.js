import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';
// Activity S54
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register() {

    // Activity S54
    const {user} = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button will be enabled or not
    const [isActive, setIsActive] = useState(false);


    // Function to simulate user registration
    function registerUser(e) {
        // Prevents page from reloading
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            }) 
        })
        .then(res => res.json())
        .then(data => {
        console.log(data);
        if(data === true){
            Swal.fire({
                title: "Duplicate email found",
                icon: "error",
                text: "Please provide a different email"
                })
            setEmail('');
        }
        else{
        

        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNumber,
                password: password1
            })            
        })
        .then(res => res.json())
        .then(data => {
        console.log(data)

            // If no user information is found, the "access" property will not be available and return undefined

            if(typeof data.access !== null){
                Swal.fire({
                    title: "Registration Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            } else {
                    Swal.fire({
                    title: "Registration Failed",
                    icon: "error",
                    text: "Please, register again."
                    })
                }
            })

        // Clear input fields
        setFirstName('');
        setLastName('');
        setEmail('');
        setMobileNumber('');
        setPassword1('');
        setPassword2('');
    }
    })

   }


    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            if(mobileNumber.length !== 11){
                setMobileNumber('');
                setPassword1('');
                setPassword2('');
                alert("Please input 11 digit mobile number")
            }else{
            setIsActive(true);}
        } else {
            setIsActive(false);
        }
    // Dependencies
    // No dependencies - effect function will run every time component renders
    // With dependency (empty array) - effect function will only run (one time) when the component renders
    // With dependencies - effect function will run anytime one of the values in the array of dependencies changes
    }, [firstName, lastName, email, mobileNumber, password1, password2]);

    return (
        // Activity S54
        (user.id !== null) ?
        <Navigate to ="/courses"/>
        :
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="firstName" 
                    placeholder="Enter First Name"
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)} 
                    required
                />

            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="lastName" 
                    placeholder="Enter Last Name"
                    value={lastName}
                    onChange={e => setLastName(e.target.value)} 
                    required
                />

            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="mobileNumber" 
                    placeholder="Enter Mobile Number"
                    value={mobileNumber}
                    onChange={e => setMobileNumber(e.target.value)} 
                    required

                />

            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>
            {/*conditionally render submit button based on "isActive" state*/}
            { isActive ?
            <Button variant="danger" type="submit" id="submitBtn">
                Submit
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
            </Button>
            }
            
        </Form>
    )
}
